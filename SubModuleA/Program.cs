﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubModuleA
{
  class Program
  {
    public static bool Flag { get; set; }

    static void Main(string[] args)
    {
      Console.WriteLine("SubModule A");
      Flag = true;
    }
  }
}
